using System.ComponentModel.DataAnnotations;

using NoteShareAPI.Interfaces;

namespace NoteShareAPI.Models;

public class LoginRequest : ILoginRequest
{
	[Required]
	public string Username { get; set; }

	[Required]
	public string Password { get; set; }
}
