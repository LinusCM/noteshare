using System.ComponentModel.DataAnnotations;

using NoteShareAPI.Interfaces;
using NoteShareAPI.Entities;

namespace NoteShareAPI.Models;

public class RegisterUser : ILoginRequest
{
	[Required]
	public string Username { get; set; }

	[Required]
	[MinLength(16)]
	public string Password { get; set; }

	[Required]
	[EmailAddress]
	public string Email { get; set; }

	[Required]
	public UserRole Role { get; set; }
}
