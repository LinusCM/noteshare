using NoteShareAPI.Interfaces;
using NoteShareAPI.Exceptions;
using NoteShareAPI.Entities;

namespace NoteShareAPI.Services;

public class NoteService : INoteService
{
    private readonly NoteContext _dbContext;

    public NoteService(NoteContext dbContext)
    {
        _dbContext = dbContext;
    }

    /// <summary>
    /// Adds a new note to the database if a note with the same name and creator does not already exist.
    /// </summary>
    /// <param name="note">The note to be added.</param>
    /// <exception cref="NoteAlreadyExistsException">Thrown if a note with the same name and creator already exists in the database.</exception>
    public void AddNote(Note note)
    {
        // Find note based on creator/note name
        Note? fetchedNote = _dbContext.Notes.FirstOrDefault(dbNote => dbNote.NoteName == note.NoteName && dbNote.Owner == note.Owner);

        if (fetchedNote != null)
            throw new NoteAlreadyExistsException($"Note {note.NoteName} by creator {note.Owner} already exists.");

        _dbContext.Notes.Add(note);
        _dbContext.SaveChanges();
    }

    /// <summary>
    /// Deletes a note if the user provided is the owner.
    /// </summary>
    /// <param name="noteName">The name of the note to be deleted.</param>
    /// <param name="user">The user attempting to delete the note.</param>
    /// <exception cref="NoteNotFoundException">Thrown if the specified note is not found in the database.</exception>
    /// <exception cref="UserNotPermittedToModifyException">Thrown if the user is not permitted to modify the specified note.</exception>
    public void DeleteNote(string noteName, User user)
    {
        Note fetchedNote = this.FetchNoteIfOwner(noteName, user);

        _dbContext.Notes.Remove(fetchedNote);
        _dbContext.SaveChanges();
    }

    /// <summary>
    /// Updates a note with the specified information for the given user, if said user is permitted.
    /// </summary>
    /// <param name="note">The updated note information.</param>
    /// <param name="user">The user attempting to update the note.</param>
    /// <exception cref="NoteNotFoundException">Thrown if the specified note is not found.</exception>
    /// <exception cref="UnauthorizedNoteAccessException">Thrown if the user does not have access to edit the note.</exception>
    public void UpdateNote(Note note, User user)
    {
        // Find note based on creator/note name
        Note? fetchedNote = _dbContext.Notes.FirstOrDefault(dbNote => dbNote.NoteName == note.NoteName && dbNote.Owner == user.Username);

        if (fetchedNote == null)
            throw new NoteNotFoundException("The note could not be found in the database.");

        // Verify access rights
        foreach (NoteMember noteMember in fetchedNote.NoteMembers)
        {
            if (noteMember.User.Username == user.Username && noteMember.Role == NoteRole.Editor)
            {
                _dbContext.Notes.Update(fetchedNote);
                _dbContext.SaveChanges();
            }
        }

        throw new UnauthorizedNoteAccessException("User does not have access to edit the note.");
    }

    /// <summary>
    /// Fetches a note with the specified name for the given user, if the user is permitted.
    /// </summary>
    /// <param name="noteName">The name of the note to be fetched.</param>
    /// <param name="user">The user attempting to fetch the note.</param>
    /// <returns>The fetched note if found and the user has access rights.</returns>
    /// <exception cref="NoteNotFoundException">Thrown if the specified note is not found in the database.</exception>
    /// <exception cref="UnauthorizedNoteAccessException">Thrown if the user does not have access to view/edit the note.</exception>
    public Note FetchNote(string noteName, User user)
    {
        // Find note based on creator/note name
        Note? fetchedNote = _dbContext.Notes.FirstOrDefault(dbNote => dbNote.NoteName == noteName && dbNote.Owner == user.Username);

        if (fetchedNote == null)
            throw new NoteNotFoundException("The note could not be found in the database.");

        // Verify access rights
        foreach (NoteMember noteMember in fetchedNote.NoteMembers)
        {
            if (noteMember.User.Username == user.Username)
                return fetchedNote;
        }

        throw new UnauthorizedNoteAccessException("User does not have access to view/edit the note.");
    }

    /// <summary>
    /// Fetches a note with the specified name for the given user, if the user it the owner of the note.
    /// </summary>
    /// <param name="noteName">The name of the note to be fetched.</param>
    /// <param name="user">The user attempting to fetch the note.</param>
    /// <returns>The fetched note if found, and the user is the owner.</returns>
    /// <exception cref="NoteNotFoundException">Thrown if the specified note is not found in the database.</exception>
    /// <exception cref="UserNotPermittedToModifyException">Thrown if the user is not the owner of the specified note.</exception>
    public Note FetchNoteIfOwner(string noteName, User user)
    {
        // Find note based on creator/note name
        Note? fetchedNote = _dbContext.Notes.FirstOrDefault(dbNote => dbNote.NoteName == noteName && dbNote.Owner == user.Username);

        if (fetchedNote == null)
            throw new NoteNotFoundException("The note could not be found in the database.");

        // Return note if user is the owner
        foreach (NoteMember noteMember in fetchedNote.NoteMembers)
        {
            if (noteMember.User.Username == user.Username && fetchedNote.Owner == user.Username)
                return fetchedNote;
        }

        throw new UserNotPermittedToModifyException($"User {user.Username} is not permitted to modify the note {noteName}.");
    }

    public void AddUserToNote(string noteName, string usernameToAdd, User managingUser) { }

    public void RemoveUserFromNote(string noteName, string usernameToRemove, User managingUser) { }

    public void ChangeUserRoleForNote(string noteName, string usernameToChange, User managingUser) { }
}
