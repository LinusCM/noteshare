using NoteShareAPI.Interfaces;
using NoteShareAPI.Exceptions;
using NoteShareAPI.Entities;

namespace NoteShareAPI.Services;

public class UserService : IUserService
{
	private readonly NoteContext _dbContext;

	public UserService(NoteContext dbContext)
	{
		_dbContext = dbContext;
	}

	/// <summary>
	/// Adds a new user to the database.
	/// </summary>
	/// <param name="user">The user to be added.</param>
	/// <exception cref="UserAlreadyExistsException">Thrown if the user with the specified username already exists in the database.</exception>
	public void AddUser(User user)
	{
		try
		{
			this.FetchUser(user.Username);
		}
		catch (UserNotFoundException)
		{
			_dbContext.Users.Add(user);
			_dbContext.SaveChanges();

			return;
		}

		throw new UserAlreadyExistsException("The username is already in use.");
	}

	/// <summary>
	/// Deletes a user from the database based on a username.
	/// </summary>
	/// <param name="username">The username of the user to be deleted.</param>
	/// <exception cref="UserNotFoundException">Thrown if the specified user does not exist in the database.</exception>
	public void DeleteUser(string username)
	{
		User fetchedUser = this.FetchUser(username);

		_dbContext.Users.Remove(fetchedUser);
		_dbContext.SaveChanges();
	}

	/// <summary>
	/// Updates an existing user in the database.
	/// </summary>
	/// <param name="user">The new user information.</param>
	/// <exception cref="UserNotFoundException">Thrown if the specified user does not exist in the database.</exception>
	public void UpdateUser(User user)
	{
		// Rethrow UserNotFoundException
		this.FetchUser(user.Username);

		_dbContext.Users.Update(user);
		_dbContext.SaveChanges();
	}

	/// <summary>
	/// Retrieves a user from the database based on a username.
	/// </summary>
	/// <param name="username">The username of the user to be fetched.</param>
	/// <returns>The fetched user.</returns>
	/// <exception cref="UserNotFoundException">Thrown if the user with the specified username is not found in the database.</exception>
	public User FetchUser(string username)
	{
		User? fetchedUser = _dbContext.Users.FirstOrDefault(user => user.Username == username);

		if (fetchedUser == null)
			throw new UserNotFoundException("The user could not be found in the database.");

		return fetchedUser;
	}
}
