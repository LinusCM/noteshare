using Microsoft.EntityFrameworkCore;
using NoteShareAPI.Entities;

public class NoteContext : DbContext
{
	public DbSet<User> Users { get; set; }
	public DbSet<Note> Notes { get; set; }

	protected readonly IConfiguration Configuration;

	public NoteContext(IConfiguration configuration)
	{
		Configuration = configuration;
	}

	protected override void OnConfiguring(DbContextOptionsBuilder options)
		=> options.UseNpgsql(Configuration.GetConnectionString("APIDatabase"));
}
