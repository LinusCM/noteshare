using System.Security.Cryptography;
using System.Text;

public class PasswordCrypto
{
	/// <summary>
	/// Generates a cryptographically secure random salt of a specified length.
	/// </summary>
	/// <param name="saltLength">Length of the salt. Default is 16 bytes.</param>
	/// <returns>An array of bytes representing the salt.</returns>
	public static byte[] GenerateSalt(int saltLength = 16)
	{
		byte[] saltBytes = new byte[saltLength];

		using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
		{
			rng.GetBytes(saltBytes);
		}

		return saltBytes;
	}

	/// <summary>
	/// Hashes a password using PBKDF2 with a specified salt, number of iterations, and key length.
	/// </summary>
	/// <param name="password">The password to be hashed.</param>
	/// <param name="salt">The salt used for hashing.</param>
	/// <param name="iterations">Hashing iterations with the salt. Default is 100,000.</param>
	/// <param name="keyLength">The length of the key in bytes. Default is 32 bytes.</param>
	/// <returns>The hashed password as a hex string with the format "salt:hash".</returns>
	public static string HashPassword(
		string password,
		byte[] salt,
		int iterations = 100000,
		int keyLength = 32)
	{
		var hash = Rfc2898DeriveBytes.Pbkdf2(
			Encoding.UTF8.GetBytes(password),
			salt,
			iterations,
			HashAlgorithmName.SHA512,
			keyLength
		);

		// Hash will be stored as salt:hash in hex 
		string saltString = Convert.ToHexString(salt);
		string hashString = Convert.ToHexString(hash);

		return saltString + ":" + hashString;
	}

	/// <summary>
	/// Validates a password against a hashed password.
	/// </summary>
	/// <param name="password">The password to be validated.</param>
	/// <param name="hashedPassword">The hashed password, including the salt, in the format of "salt:hash"</param>
	/// <returns>True if the password matches; otherwise, false.</returns>
	public static bool ValidatePassword(string password, string hashedPassword)
	{
		string[] seperatedHash = hashedPassword.Split(":");

		string newHashedPassword = HashPassword(
			password,
			Convert.FromHexString(seperatedHash[0])
		);

		return newHashedPassword == hashedPassword;
	}
}
