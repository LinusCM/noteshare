using NoteShareAPI.Entities;

namespace NoteShareAPI.Interfaces;

public interface INoteService
{
    // Note editing
    void AddNote(Note note);

    void DeleteNote(string noteName, User user);

    void UpdateNote(Note note, User user);

    Note FetchNote(string noteName, User user);

    Note FetchNoteIfOwner(string noteName, User user);

    // Note management
    void AddUserToNote(string noteName, string usernameToAdd, User managingUser);

    void RemoveUserFromNote(string noteName, string usernameToRemove, User managingUser);

    void ChangeUserRoleForNote(string noteName, string usernameToChange, User managingUser);
}
