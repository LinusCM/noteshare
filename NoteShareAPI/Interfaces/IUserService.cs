using NoteShareAPI.Entities;

namespace NoteShareAPI.Interfaces;

public interface IUserService
{
	void AddUser(User user);

	void DeleteUser(string username);

	void UpdateUser(User user);

	User FetchUser(string username);
}
