using Microsoft.AspNetCore.Mvc;

using NoteShareAPI.Interfaces;
using NoteShareAPI.Services;
using NoteShareAPI.Models;
using NoteShareAPI.Entities;
using NoteShareAPI.Exceptions;

namespace NoteShareAPI.Controllers;

[ApiController]
[Route("register")]
public class RegisterController : ControllerBase
{
	public IActionResult Register([FromBody] RegisterUser registerUser)
	{

		using (NoteContext userContext = new NoteContext(Configuration.GetConfig()))
		{
			IUserService userService = new UserService(userContext);

			// Make sure user doesn't already exist.
			try
			{
				// Hash password.
				byte[] salt = PasswordCrypto.GenerateSalt();
				string hashedPassword = PasswordCrypto.HashPassword(registerUser.Password, salt);

				// Register new user in DB.
				User newUser = new User();

				newUser.Username = registerUser.Username;
				newUser.HashedPassword = hashedPassword;
				newUser.Email = registerUser.Email;
				newUser.Role = registerUser.Role;

				userService.AddUser(newUser);

			}
			catch (UserAlreadyExistsException)
			{
				return Conflict("User already exists");
			}

			return NoContent();
		}
	}
}
