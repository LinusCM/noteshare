using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;

using NoteShareAPI.Interfaces;
using NoteShareAPI.Services;
using NoteShareAPI.Models;
using NoteShareAPI.Entities;
using NoteShareAPI.Exceptions;

namespace NoteShareAPI.Controllers;

[ApiController]
[Route("login")]
public class LoginController : ControllerBase
{
	// JWT values
	private static readonly IConfiguration JwtConfig = Configuration.GetConfig().GetSection("JWT");
	private static readonly SymmetricSecurityKey JwtKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtConfig["Key"] ?? throw new Exception("Invalid appsettings.json")));
	private static readonly double JwtLifetime = Double.Parse(JwtConfig["Lifetime"] ?? throw new Exception("Invalid appsettings.json"));

	[HttpPost]
	public IActionResult Login([FromBody] LoginRequest loginRequest)
	{
		using (NoteContext userContext = new NoteContext(Configuration.GetConfig()))
		{
			IUserService userService = new UserService(userContext);

			User? fetchedUser;

			// Check if user exists
			try
			{
				fetchedUser = userService.FetchUser(loginRequest.Username);
			}
			catch (UserNotFoundException)
			{
				return BadRequest($"Password or username is invalid.");
			}

			// Validate password
			bool isPasswordValid = PasswordCrypto.ValidatePassword(loginRequest.Password, fetchedUser.HashedPassword!);

			if (fetchedUser == null || isPasswordValid == false)
			{
				return BadRequest($"Password or username is invalid.");
			}
		}

		// Create token.
		var claims = new[]
		{
			new Claim(JwtRegisteredClaimNames.Sub, loginRequest.Username),
		};

		string tokenString;

		// Can fail if fields are missing, e.g. Key.
		try
		{
			var token = new JwtSecurityToken(
				claims: claims,
				expires: DateTime.UtcNow.AddMinutes(JwtLifetime),
				signingCredentials: new SigningCredentials(JwtKey, SecurityAlgorithms.HmacSha256)
			);

			// Serialize token.
			tokenString = new JwtSecurityTokenHandler().WriteToken(token);
		}
		catch (ArgumentNullException)
		{
			return StatusCode(500);
		}

		return Ok(new { Token = tokenString });
	}
}
