namespace NoteShareAPI.Exceptions;

using System;


public class UserNotPermittedToModifyException : Exception
{
	public UserNotPermittedToModifyException()
	{
	}

	public UserNotPermittedToModifyException(string message)
		: base(message)
	{
	}

	public UserNotPermittedToModifyException(string message, Exception inner)
		: base(message, inner)
	{
	}
}
