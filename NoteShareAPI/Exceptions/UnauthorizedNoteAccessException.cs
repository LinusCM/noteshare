namespace NoteShareAPI.Exceptions;

using System;


public class UnauthorizedNoteAccessException : Exception
{
	public UnauthorizedNoteAccessException()
	{
	}

	public UnauthorizedNoteAccessException(string message)
		: base(message)
	{
	}

	public UnauthorizedNoteAccessException(string message, Exception inner)
		: base(message, inner)
	{
	}
}
