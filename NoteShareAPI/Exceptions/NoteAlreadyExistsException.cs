namespace NoteShareAPI.Exceptions;

using System;


public class NoteAlreadyExistsException : Exception
{
    public NoteAlreadyExistsException()
    {
    }

    public NoteAlreadyExistsException(string message)
        : base(message)
    {
    }

    public NoteAlreadyExistsException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
