namespace NoteShareAPI.Entities;

public enum NoteRole
{
	Editor,
	Viewer,
}
