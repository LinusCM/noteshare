namespace NoteShareAPI.Entities;

public class Note
{
	public int NoteId { get; set; }
	public string NoteName { get; set; }
	public string Owner { get; set; }
	public string Content { get; set; } = "";
	public NoteMember[] NoteMembers { get; set; }
}
