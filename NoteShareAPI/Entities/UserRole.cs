namespace NoteShareAPI.Entities;

public enum UserRole
{
	User,
	Admin,
}
