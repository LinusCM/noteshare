namespace NoteShareAPI.Entities;

public class NoteMember
{
	public int NoteAccessId { get; set; }
	public User User { get; set; }
	public NoteRole Role { get; set; }
	public DateTime? TimePeriod { get; set; }
}
