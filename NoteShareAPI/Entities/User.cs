namespace NoteShareAPI.Entities;

public class User
{
	public int UserId { get; set; }
	public string Username { get; set; }
	public string HashedPassword { get; set; }
	public string Email { get; set; }
	public UserRole Role { get; set; }
}
