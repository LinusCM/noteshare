# NoteShare
A simple note sharing application, for collaboration and alike.


## API Diagram
```mermaid
  
```

## Endpoints
### A list of endpoints for the API:

Login:
``/login``

Registration:
``/register``

List notes:
``/notes``

Get certain note:
``/notes/my_note``

To give another user access, add the following to the body
of your request:
```json
{
  "UsersWithAccess": {
    "user01": {
      "Role": "Editor",
    },
    "user02": {
      "Role": "Viewer",
      "TimePeriod": "[Timestamp]"
    },
  }
}
```

Where the roles:
- ``Editor`` can edit the note.
- ``Viewer`` can view the note.

With ``Time`` being the period you want to give the user access for.
To make a users access indefinite, leave this field out.

Then sent to the endpoint:
``/notes/my_note/addusers``


## Dependencies
- [PostgresSQL](https://www.postgresql.org/)
- [.NET Core](https://dotnet.microsoft.com/en-us/download)
  - [EntityFrameworkCore](https://www.nuget.org/packages/Npgsql.EntityFrameworkCore.PostgreSQL/)
  - [JWTBearer](https://www.nuget.org/packages/Microsoft.AspNetCore.Authentication.JwtBearer)
  - [Swashbuckle.AspNetCore](https://www.nuget.org/packages/Swashbuckle.AspNetCore)
